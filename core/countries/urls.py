from django.urls import path
from .views import DepartamentsView,MunicipalitiesView


urlpatterns = [
    path('<int:id>',DepartamentsView.as_view(),name='departamentos'),
    path('municipios/<int:id>',MunicipalitiesView.as_view(),name='municipios'),
    ]