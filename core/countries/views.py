from typing import Any
from django import http
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from .models import Country,Departament,Municipality

class HomeView(TemplateView):
    template_name='home/home.html'
    
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self,request):
        data={}
        try:
            countries=Country.objects.all()
            data['countries']=countries
            data['error']=0
        except:
            data['error']=1
        return render(request,self.template_name,data)

class DepartamentsView(TemplateView):
    template_name='country/departaments.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request,id=0):
        data={}
        if id!=0:
            data['coun_id']=id
            try:
                coun=Country.objects.get(pk=id)
                data['coun_name']=coun.name
                departaments=Departament.objects.filter(country=id)
                data['departaments']=departaments
                exists=len(departaments) > 0
                if exists:
                    data['error']=0
                else:
                    raise 'error'
            except:
                data['error']=1
        return render(request,self.template_name,data)
    
class MunicipalitiesView(TemplateView):
    template_name='departaments/municipalities.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request,id=0):
        data={}
        if id !=0:
            data['depa_id']=id
            
            try:
                depa=Departament.objects.get(pk=id)
                data['depa_name']=depa.name
                municipalities=Municipality.objects.filter(departament=id)
                data['municipalities']=municipalities
                exists=len(municipalities)>0
                if exists:
                    data['error']=0
                else:
                    raise 'error'
            except:
                data['error']=1
        return render(request,self.template_name,data)