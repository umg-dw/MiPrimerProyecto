from django.contrib import admin
from django.utils.safestring import mark_safe
from .models import Country,Departament,Municipality

@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display=['name']
    list_filter=[]
    list_editable=[]
    list_per_page=15
    search_fields=['name']

@admin.register(Departament)
class DepartamentAdmin(admin.ModelAdmin):
    list_display=['name']
    list_filter=['country']
    list_editable=[]
    list_per_page=15
    search_fields=['name','country']

@admin.register(Municipality)
class MunicipalityAdmin(admin.ModelAdmin):
    list_display=['name']
    list_filter=['departament']
    list_editable=[]
    list_per_page=15
    search_fields=['name','departament']