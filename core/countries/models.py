from django.db import models

class Country(models.Model):
    name=models.CharField(db_column='NAME',verbose_name='Nombre',max_length=70)

    class Meta:
        db_table='COUNTRY'
        verbose_name='País'
        verbose_name_plural='Países'
    
    def __str__(self):
        return f'{self.name}'
    
class Departament(models.Model):
    name=models.CharField(db_column='NAME',verbose_name='Nombre',max_length=100)
    country=models.ForeignKey(Country,db_column='COUNTRY',verbose_name='País',on_delete=models.CASCADE)

    class Meta:
        db_table='DEPARTAMENT'
        verbose_name='Departamento'
        verbose_name_plural='Departamentos'
    
    def __str__(self):
        return f'{self.name} de {self.country}'
    
class Municipality(models.Model):
    name=models.CharField(db_column='NAME',verbose_name='Nombre',max_length=120)
    departament=models.ForeignKey(Departament,db_column='DEPARTAMENT',verbose_name='Municipio',on_delete=models.CASCADE)

    class Meta:
        db_table='MUNICIPALITY'
        verbose_name='Municipalidad'
        verbose_name_plural='Municipalidades'
    
    def __str__(self):
        return f'{self.name}, {self.departament}'